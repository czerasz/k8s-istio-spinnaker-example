# Kubernetes, Istio, Spinnaker Example

## Task Definition

- Setup a K8S cluster

  The plan is to deploy an EKS cluster in a dedicated VPC. I would like to use the following Terraform module:

  - [terraform-aws-eks-cluster](https://github.com/cloudposse/terraform-aws-eks-cluster)

- Deploy istio with Helm

  After a short research I found out that it's recommended to use [`istioctl`](https://istio.io/latest/docs/reference/commands/istioctl/) to setup.
  Since it's recommended I will use it.

- Deploy spinnaker with Helm

  There is a [Helm chart for Spinnaker](https://hub.helm.sh/charts/stable/spinnaker) in the official Helm repository. This Helm chart is using [`hal`](https://spinnaker.io/setup/install/halyard/) to setup the Spinnaker ecosystem.

- Deploy a webservice with Helm

  The application which will be deployed and the Helm chart were prepared before:

  - [Sample Application](https://gitlab.com/czerasz/sample-app-with-failure-and-start-delay/)
  - [Helm Chart](https://gitlab.com/czerasz/sample-app-helm-chart/)

- Simulate a canary release of this deployment

  The plan is to setup the pipelines through the UI and then add the exported JSON files to the repository,

## Development Environment

I will need the tools:

- `terraform` - to setup the EKS cluster
- `kubectl` - to interact with the EKS cluster
- `istioctl` - to setup istio on the EKS cluster
- `helm` - to deploy Spinnaker
- `aws-iam-authenticator` - needed to authenticate while accessing the EKS cluster
- something to simulate the trafic... maybe [vegeta](https://github.com/tsenart/vegeta/)
- `docker`

  ~~Maybe docker will **NOT** be necessary for full isolation. Looking at the software mentioned above... Everything is delivered as static binaries, so maybe Docker will be obsolete.~~

  Docker is required to fetch the initial `kubeconfig`.

There required tools can be downloaded on an linux machine with `./scripts/download-tools`. With a little effort one can tweak this script to work on MacOS.

For easier access to those tools execute `export PATH="${PWD}/bin:${PATH}"` in each new terminal.

## Setup EKS cluster with Terraform

- I started with creating a separate AWS account for this project
- AWS access:

  To access AWS via command line I manually created a "terraform" IAM user with attached "AdministratorAccess" policy and then allowed it for generic access.

  The access key and secret I store in `./secrets` (which is ignored by git):

  ```bash
  $ cat ./secrets
  export AWS_ACCESS_KEY_ID="<secret>"
  export AWS_SECRET_ACCESS_KEY="<secret>"
  ```

  To use those secrets simply:

  ```bash
  source ./secrets
  ```

- setup state S3 bucket and DynamoDB lock for Terraform

  This will allow to execute Terraform from the CI in the future and allow working with multiple team members.

  The directory structure looks like this:

  ```
  terraform
  └── env
      └── dev
          └── aws
              └── eu-central-1
                  ├── region.auto.tfvars
                  └── state
                      ├── main.tf
                      ├── output.tf
                      ├── providers.tf
                      ├── region.auto.tfvars -> ../region.auto.tfvars
                      └── variables.tf
  ```

  To roll it out change the directory and then perform necessary Terraform commands:

  ```bash
  $ cd terraform/env/dev/aws/eu-central-1/state
  $ terraform init
  $ terraform plan
  $ terraform apply -auto-approve
  ```

  To view the backend configuration which will be useful in the near future use the following command:

  ```bash
  $ terraform output -json | jq -r '.dev_state.value.terraform_backend_config'
  terraform {
    required_version = ">= 0.12.2"

    backend "s3" {
      region         = "eu-central-1"
      bucket         = "czerasz-dev-terraform-state-k8s-istio-spinnaker-example"
      key            = "terraform.tfstate"
      dynamodb_table = "czerasz-dev-terraform-state-k8s-istio-spinnaker-example-lock"
      profile        = ""
      role_arn       = ""
      encrypt        = "true"
    }
  }
  ```

- setup EKS with the researched Terraform modules

  The directory structure looks like this:

  ```
  terraform
  └── env
      └── dev
          ├── aws
          │   └── eu-central-1
          │       ├── eks
          │       │   ├── backend.tf
          │       │   ├── context.tf
          │       │   ├── environment.auto.tfvars -> ../../../environment.auto.tfvars
          │       │   ├── main.tf
          │       │   ├── outputs.tf
          │       │   ├── providers.tf
          │       │   ├── region.auto.tfvars -> ../region.auto.tfvars
          │       │   ├── terraform.tfvars
          │       │   └── variables.tf
          │       ├── region.auto.tfvars
          │       └── state
          └── environment.auto.tfvars
  ```

  It's mostly based on the [example](https://github.com/cloudposse/terraform-aws-eks-node-group/blob/master/examples/complete/main.tf) provided in the `github.com/cloudposse/terraform-aws-eks-node-group` repository

  I improved couple of details but those were mostly small changes.

  To setup the EKS cluster I will use:

  ```bash
  $ cd terraform/env/dev/aws/eu-central-1/eks
  $ terraform init
  $ terraform plan
  $ terraform apply -auto-approve
  ```

  Terraform will create:
  - a dedicated VPC
  - the VPC will have public and private subnets
  - the private subnet can access internet via NAT gateway
  - EKS cluster
  - launch template
  - 2 EC2 instances in an ASG which will be our worker nodes

  > **NOTE**
  >
  > While deploying I'm thinking if the capacity for what we plan is enough. Currently the instance type `t3.small` which might be a little bit too small.

  > **NOTE**
  >
  > The `terraform apply` took about 30 minutes

- after the EKS cluster is crated we need to connect to it

  From the last `terraform apply` we got the cluster ID: `czerasz-dev-istio-spinnaker-example-cluster`. The quickest way to access the cluster is getting a kubeconfig via the `aws-cli`. `aws-cli` is python so it might be our only change to use Docker.

  Create a container:

  ```bash
  docker run --rm -it -v $PWD/:/project --workdir /project alpine:3 sh
  ```

  > **NOTE**
  >
  > Do **NOT** forget to "mount" the AWS credentials file

  Now install inside `aws-cli`:

  ```bash
  apk add --update aws-cli
  ```

  Then "load" the AWS credentials:

  ```bash
  source secrets
  ```

  Then fetch the `kubeconfig`:

  ```bash
  $ aws eks --region eu-central-1 update-kubeconfig --name czerasz-dev-istio-spinnaker-example-cluster --kubeconfig ./kubeconfig
  $ chown 1000:1000 ./kubeconfig
  ```

  Leave the container:

  ```bash
  exit
  ```

  There is one more step - add `aws-iam-authenticator` and modify the `kubeconfig` to use it instead of the `aws-cli`. I added the `aws-iam-authenticator` to the `scripts/download-tools` script. Download it:

  ```bash
  scripts/download-tools
  ```

  Now adjust the `kubeconfig` - change the `exec` block to:

  ```yaml
  exec:
    apiVersion: client.authentication.k8s.io/v1alpha1
    args:
    - token
    - "-i"
    - czerasz-dev-istio-spinnaker-example-cluster
    command: aws-iam-authenticator
  ```

  Try it out:

  ```bash
  $ kubectl --kubeconfig=./kubeconfig get ns
  NAME              STATUS   AGE
  default           Active   29m
  kube-node-lease   Active   29m
  kube-public       Active   29m
  kube-system       Active   29m
  ```

  To skip unecessary typing `--kubeconfig=./kubeconfig` execute

  ```bash
  export KUBECONFIG="${PWD}/kubeconfig"
  ```

  **Well done we are in!**

- as a warmup lets install something small but useful - `metrics-server`

  Without the `metrics-server` we are unable to see how our nodes are performing:

  ```bash
  $ kubectl top nodes                    
  error: Metrics API not available
  ```

  We begin with adding the stable Helm repository:

  ```bash
  $ helm repo add stable https://kubernetes-charts.storage.googleapis.com/
  "stable" has been added to your repositories
  ```

  Next install the `metrics-server`:

  ```bash
  $ kubectl create ns metrics
  $ helm install --namespace metrics metrics-server stable/metrics-server --dry-run
  $ helm install --namespace metrics metrics-server stable/metrics-server
  ```

  After a couple of seconds we can observe the node stats:

  ```bash
  $ kubectl top nodes
  NAME                                            CPU(cores)   CPU%   MEMORY(bytes)   MEMORY%
  ip-10-20-17-101.eu-central-1.compute.internal   55m          2%     444Mi           30%
  ip-10-20-63-151.eu-central-1.compute.internal   54m          2%     427Mi           28%
  ```

## Setup istio via `istioctl`

Do the pre-check:

```bash
$ istioctl experimental precheck
```

List available setup profiles:

```bash
$ istioctl profile list
Istio configuration profiles:
  default
  demo
  empty
  minimal
  preview
  remote
```

Dump the "demo" profile:

```bash
istioctl profile dump demo > istio.profile.yaml
```

Install istio based on the "demo" profile:

```bash
$ istioctl install -f istio.profile.yaml --dry-run
$ istioctl install -f istio.profile.yaml
✔ Istio core installed
✔ Istiod installed
✔ Egress gateways installed
✔ Ingress gateways installed
✔ Addons installed
✔ Installation complete
```

Verify the installation was successful:

```bash
istioctl verify-install -f istio.profile.yaml
```

> **NOTE**
>
> With the YAML installation file we can controll the istio environment with an IaC feeling which can be also reproduced in CI.


One can also compare specific resources created through differnt profiles

```bash
$ istioctl manifest generate -f istio.profile.yaml > istio.manifest.yml
$ istioctl manifest generate --set profile=minimal > istio.manifest.minimal.yml
$ diff --color -c istio.manifest.minimal.yml istio.manifest.yml
```

## Setup Spinnaker via Helm

Search for Spinnaker:

```bash
$ helm search repo spinnaker
NAME              CHART VERSION  APP VERSION  DESCRIPTION
stable/spinnaker  2.2.3          1.16.2       Open source, multi-cloud continuous delivery pl...
```

Download the Spinnaker Helm default values file:

```bash
mkdir -p helm/spinnaker
helm show values --version 2.2.3 stable/spinnaker > helm/spinnaker/values.yaml
```

> **NOTE**
>
> We need the values file because we will need to adjust it later

Create a namespace for Spinnaker:

```bash
kubectl create ns spinnaker
```

Install Spinnaker in "dry run" mode:

```bash
helm install -n spinnaker --version 2.2.3 -f helm/spinnaker/values.yaml spinnaker stable/spinnaker --dry-run
```

Unfortunatelly the command above returns an error:

```
Error: template: spinnaker/charts/minio/templates/deployment.yaml:210:20: executing "spinnaker/charts/minio/templates/deployment.yaml" at <(not .Values.gcsgateway.enabled) (not .Values.azuregateway.enabled) (not .Values.s3gateway.enabled) (not .Values.b2gateway.enabled)>: can't give argument to non-function not .Values.gcsgateway.enabled
```

Lets pull the chart locally and try to debug it:

```bash
$ helm pull --version 2.2.3 stable/spinnaker
$ tar -xf spinnaker-2.2.3.tgz
$ rm spinnaker-2.2.3.tgz
# Remove the previously downloaded file
$ rm -r helm/spinnaker
$ mv spinnaker helm/
```

Now we can debug having the files locally:

```bash
helm install -n spinnaker spinnaker --dry-run ./helm/spinnaker
```

## Fix the Issue in the Spinnaker Helm chart

It turns out that the issue is in one of the dependencies - the `minio` chart (`helm/spinnaker/charts/minio/`). Below is the patch:

```diff
diff --git a/templates/deployment.yaml b/templates/deployment.yaml
index 2b9fd5a..6c8009e 100755
--- a/templates/deployment.yaml
+++ b/templates/deployment.yaml
@@ -207,7 +207,7 @@ spec:
 {{ toYaml . | indent 8 }}
 {{- end }}
       volumes:
-        {{- if and ((not .Values.gcsgateway.enabled) (not .Values.azuregateway.enabled) (not .Values.s3gateway.enabled) (not .Values.b2gateway.enabled)) }}
+        {{- if and (not .Values.gcsgateway.enabled) (not .Values.azuregateway.enabled) (not .Values.s3gateway.enabled) (not .Values.b2gateway.enabled) }}
         - name: export
         {{- if .Values.persistence.enabled }}
           persistentVolumeClaim:
```

It also turns out that the issue was already fixed [here](https://github.com/helm/charts/commit/89a712556d2e3bb8c18acc350dca0bc9a5baba8d). Our fix is as easy as bumping the `minio` dependency version to `>=5.0.16`. The highest version as of the time of writing is `5.0.33`.

The PR with the fix was submited [here](https://github.com/helm/charts/pull/23814).

Unfortunatelly there is no quick and transparent way to use code from the mentioned PR. That's why the spinnaker chart was directly to `./helm/spinnaker`.

Now we can install spinnaker:

```bash
helm install -n spinnaker spinnaker ./helm/spinnaker --timeout 600s
```

> **NOTE**
>
> Increase the timeout with `--timeout 600s` as recommended in the documentation.

## Resize Cluster

Unfortunately during Spinnaker rollout something which we already expected happened:

```bash
$ kubectl get nodes
NAME                                            STATUS     ROLES    AGE     VERSION
ip-10-20-17-101.eu-central-1.compute.internal   NotReady   <none>   5h17m   v1.17.9-eks-4c6976
ip-10-20-63-151.eu-central-1.compute.internal   Ready      <none>   5h17m   v1.17.9-eks-4c6976
```

It seems that the cluster is to small.

One worker node will be added:

```diff
$ diff --git a/terraform/env/dev/aws/eu-central-1/eks/terraform.tfvars b/terraform/env/dev/aws/eu-central-1/eks/terraform.tfvars
index ae8daad..cea2b22 100644
--- a/terraform/env/dev/aws/eu-central-1/eks/terraform.tfvars
+++ b/terraform/env/dev/aws/eu-central-1/eks/terraform.tfvars
@@ -13,11 +13,11 @@ cluster_log_retention_period = 7
 
 instance_types = ["t3.small"]
 
-desired_size = 2
+desired_size = 4
 
-max_size = 3
+max_size = 5
 
-min_size = 2
+min_size = 4
 
 disk_size = 20
```

Changes will be rolled out with (+ a little bit state Hokuspokus):

```bash
cd terraform/env/dev/aws/eu-central-1/eks/
terraform plan
terraform apply -auto-approve
```

Unfortunately this doesn't help:

```bash
$ kubectl top nodes
NAME                                            CPU(cores)   CPU%   MEMORY(bytes)   MEMORY%
ip-10-20-2-182.eu-central-1.compute.internal    253m         13%    1360Mi          91%
ip-10-20-30-169.eu-central-1.compute.internal   282m         14%    1521Mi          102%
ip-10-20-46-95.eu-central-1.compute.internal    104m         5%     1049Mi          70%
ip-10-20-63-151.eu-central-1.compute.internal   122m         6%     1045Mi          70%
```

We need beefier instances! That's why `eks_node_group_v2` was added. Again:

```bash
$ cd terraform/env/dev/aws/eu-central-1/eks/
$ terraform init
$ terraform plan
$ terraform apply -auto-approve
```

Next drain all old nodes:

```bash
kubectl drain -l beta.kubernetes.io/instance-type=t3.small --ignore-daemonsets --delete-local-data
```

Once this is done and all relevant pods are on the new nodes, remove the old node group:

```bash
$ cd terraform/env/dev/aws/eu-central-1/eks/
$ terraform plan
$ terraform apply -auto-approve
```

## Fine tune Spinnaker Configuration

A new `spinnaker-values.yaml` was added with the following features:

- update Docker image with halyard
- update spinnaker version
- enable canary feature
  - add minio secret to access credentials 

```diff
$ diff -U5 --color helm/spinnaker/values.yaml spinnaker-values.yaml
--- helm/spinnaker/values.yaml	1970-01-01 01:00:00.000000000 +0100
+++ spinnaker-values.yaml	2020-09-27 12:23:07.142163689 +0200
@@ -1,10 +1,10 @@
 halyard:
-  spinnakerVersion: 1.19.4
+  spinnakerVersion: 1.19.14
   image:
     repository: gcr.io/spinnaker-marketplace/halyard
-    tag: 1.32.0
+    tag: 1.39.0
     pullSecrets: []
   # Set to false to disable persistence data volume for halyard
   persistence:
     enabled: true
   # Uncomment to add storage class for the persistence data volume
@@ -12,22 +12,55 @@
   # Provide additional parameters to halyard deploy apply command
   additionalInstallParameters: []
   # Provide a config map with Hal commands that will be run the core config (storage)
   # The config map should contain a script in the config.sh key
   additionalScripts:
-    enabled: false
-    configMapName: my-halyard-config
-    configMapKey: config.sh
-    # If you'd rather do an inline script, set create to true and put the content in the data dict like you would a configmap
-    # The content will be passed through `tpl`, so value interpolation is supported.
-    create: false
-    data: {}
+    create: true
+    data:
+      enable_canary.sh: |-
+        command='add'
+        if $HAL_COMMAND config canary prometheus account get istio-prometheus; then
+          command='edit'
+        fi
+        $HAL_COMMAND config canary prometheus account "${command}" istio-prometheus --base-url http://prometheus.istio-system.svc.cluster.local:9090
+
+        $HAL_COMMAND config canary edit --default-metrics-store prometheus
+        $HAL_COMMAND config canary edit --default-metrics-account istio-prometheus
+
+        command='add'
+        if $HAL_COMMAND config canary aws account get canary-storage; then
+          command='edit'
+        fi
+        # Read secrets
+        accesskey=$(cat /opt/halyard/additionalSecrets/accesskey)
+        secretkey=$(cat /opt/halyard/additionalSecrets/secretkey)
+        $HAL_COMMAND config canary aws account "${command}" canary-storage \
+        --access-key-id ${accesskey} \
+        --secret-access-key ${secretkey} \
+        --endpoint http://spinnaker-minio.spinnaker.svc.cluster.local:9000 \
+        --bucket spinnaker \
+        --root-folder canary
+
+        $HAL_COMMAND config canary edit --default-storage-account canary-storage
+        $HAL_COMMAND config canary aws edit --s3-enabled true
+
+        $HAL_COMMAND config canary prometheus enable
+        $HAL_COMMAND config canary aws enable
+        $HAL_COMMAND config canary enable
+
+        # canary is available only for the specific app
+        # $HAL_COMMAND config canary edit --show-all-configs-enabled false
+
+        # No need for "hal deploy apply" since it will be run afterwards
+
+      enable_artifacts.sh: |-
+        $HAL_COMMAND config artifact http enable
+
+        # No need for "hal deploy apply" since it will be run afterwards

   additionalSecrets:
-    create: false
-    data: {}
-    ## Uncomment if you want to use a pre-created secret rather than feeding data in via helm.
-    # name:
+    name: spinnaker-minio
   additionalConfigMaps:
     create: false
     data: {}
     ## Uncomment if you want to use a pre-created ConfigMap rather than feeding data in via helm.
     # name:
```

Update the Helm release:

```bash
helm upgrade --install -n spinnaker spinnaker ./helm/spinnaker --timeout 600s -f spinnaker-values.yaml
```

Lets see if our nodes are stable:

```bash
$ kubectl top nodes
NAME                                            CPU(cores)   CPU%   MEMORY(bytes)   MEMORY%
ip-10-20-17-113.eu-central-1.compute.internal   122m         6%     949Mi           13%
ip-10-20-50-253.eu-central-1.compute.internal   367m         19%    3984Mi          55%
```

Indeed they are even underutilized, so we could optimize even further...

## Configure Spinnaker Pipelines via UI

To access the Spinnaker UI use:

```bash
$ export DECK_POD=$(kubectl get pods --namespace spinnaker -l "cluster=spin-deck" -o jsonpath="{.items[0].metadata.name}")
$ kubectl port-forward --namespace spinnaker $DECK_POD 9000
```

Open [http://localhost:9000/](http://localhost:9000/) in Your browser and woooohhhhooo we are in!

## Simulate Trafic

Whenever trafic flow is needed on the application I will execute the following:

- generate trafic with:

  ```bash
  export LB_HOST=a20b54931332e444695f1eda36b2c909-1065606335.eu-central-1.elb.amazonaws.com
  echo "GET http://${LB_HOST}" | ./bin/vegeta attack -duration 10m -rate '5/1s' > load-test.bin
  ```

- in another terminal watch the stress test statistics:

  ```bash
  watch -n5 -c 'cat load-test.bin | ./bin/vegeta report'
  ```

## Configure Spinnaker Application

In Spinnaker UI the following resources were configured:

- create Canary Config (`spinnaker/canary-configs/istio.json`)

  ![Canary Config](doc/img/canary-config-1.png)

  ![Canary Config - metric](doc/img/canary-config-2.png)

  To configure the Canary Config I used a modified PromQL query which was available on the preconfigured Grafana dashboard (Istio Workload Dashboard, "Incoming Success Rate (non-5xx responses)" tile):

  ```
  sum(
    irate(
      istio_requests_total{
        reporter="destination",
        connection_security_policy="mutual_tls",
        destination_workload_namespace="${location}",
        destination_workload="sample-app-${scope}",
        response_code!~"5.*"
      }[1m]
    )
  )

  /

  sum(
    irate(
      istio_requests_total{
        reporter="destination",
        connection_security_policy="mutual_tls",
        destination_workload_namespace="${location}",
        destination_workload="sample-app-${scope}"
      }[1m]
    )
  )
  ```

  ![Grafana as inspiration for canary metric](doc/img/grafana-success-rate.png)
  

  In simple words this metric tells how much traffic is "positive". In daily business our application will have this metric close to 100%. If the canary wants to pass our standards it should be also close to this mark. For demonstation purposes I lowered this level to 80% (see "Canary  50% Analysis" stage in the deploy pipeline).

- Init pipeline (`spinnaker/pipelines/init.json`)

  Responsible for the initial application setup. This pipeline sets up the following resources:
  - namespace
  - gateway
  - initial stable deployment

  ![Init Pipeline](doc/img/pipeline-init.png)

- Deploy pipeline (`spinnaker/pipelines/deploy.json`)

  This pipeline is responsible for the canary releases.

  ![Deploy Pipeline](doc/img/pipeline-canary-deploy.png)

## Testing Pipelines

Below are couple screenshots with how the canary release was tested.

### Failing Release

![Execution Parameters](doc/img/failed-release/1-deploy-config.png)
![Waiting for Canary Analysis](doc/img/failed-release/2-canary-analysis.png)
![Waiting for Canary Analysis](doc/img/failed-release/2.1-canary-analysis.png)
![Canary Analysis failed](doc/img/failed-release/3-unsatisfied-canary-analysis.png)
![Canary release statistics](doc/img/failed-release/4-canary-statistics.png)
![Stable release statistics](doc/img/failed-release/5-stable-statistics.png)
![Failed rollout](doc/img/failed-release/6-refuse-to-rollout.png)

### Successful Release

![Execution Parameters](doc/img/successful-release/1-deploy-config.png)
![Canary release statistics](doc/img/successful-release/2-canary-statistics.png)
![Successful rollout](doc/img/successful-release/3-succeeded-rollout.png)