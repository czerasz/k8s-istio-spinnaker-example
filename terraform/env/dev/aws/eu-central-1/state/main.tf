module "dev_state" {
  source = "git::https://github.com/cloudposse/terraform-aws-tfstate-backend.git?ref=tags/0.26.0"

  namespace  = "czerasz"
  stage      = var.stage
  name       = "terraform"
  attributes = ["state", "k8s-istio-spinnaker-example"]

  force_destroy = true
}