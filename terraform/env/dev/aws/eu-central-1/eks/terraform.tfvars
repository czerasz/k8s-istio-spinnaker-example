availability_zones = ["eu-central-1a", "eu-central-1b"]

namespace = "czerasz"
name = "istio-spinnaker-example"

kubernetes_version = "1.17"

oidc_provider_enabled = true

enabled_cluster_log_types = ["audit"]

cluster_log_retention_period = 7

instance_types = ["t3.large"]

desired_size = 2

max_size = 3

min_size = 2

disk_size = 20

kubernetes_labels = {}