terraform {
  required_version = ">= 0.13.3"

  backend "s3" {
    region         = "eu-central-1"
    bucket         = "czerasz-dev-terraform-state-k8s-istio-spinnaker-example"
    key            = "terraform.tfstate"
    dynamodb_table = "czerasz-dev-terraform-state-k8s-istio-spinnaker-example-lock"
    profile        = ""
    role_arn       = ""
    encrypt        = "true"
  }
}